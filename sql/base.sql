CREATE TABLE dept(
  deptno INT NOT NULL,
  dname VARCHAR(14),
  loc VARCHAR(13),
  PRIMARY KEY(deptno)
);

INSERT INTO dept VALUES(1,'accounting','一区');
INSERT INTO dept VALUES(2,'research','二区');
INSERT INTO dept VALUES(3,'operations','二区');

CREATE TABLE emp(
  empno INT NOT NULL,
  ename VARCHAR(10),
  job VARCHAR(9),
  mgr NUMERIC(4),
  hiredate DATE,
  sal NUMERIC(7,2),
  comm NUMERIC(7,2),
  deptno INT,
  PRIMARY KEY(empno)
);

INSERT INTO emp VALUES(100,'jack','副总',NULL,'2002-05-03',90000,NULL,1);
INSERT INTO emp VALUES(200,'tony','总监',100,'2015-02-02',10000,2000,2);
INSERT INTO emp VALUES(300,'hana','经理',200,'2017-02-02',8000,1000,2);
INSERT INTO emp VALUES(400,'leo','员工',300,'2019-02-22',3000,200.12,2);
INSERT INTO emp VALUES(500,'liu','员工',300,'2019-03-19',3500,200.58,2);

CREATE TABLE `empext` (
  `empno` INT NOT NULL,
  `address` VARCHAR(300) DEFAULT NULL,
  `phone` VARCHAR(20) DEFAULT NULL,
  `cardno` VARCHAR(20) DEFAULT NULL,
  PRIMARY KEY (`empno`)
);
